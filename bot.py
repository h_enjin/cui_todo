# coding: utf-8
import sys, sqlite3, datetime

dbcon = sqlite3.connect('todo.db')
dbcur = dbcon.cursor()
today = datetime.datetime.today()


# メニュー関数
def menu(argument):
	# 0は全タスクチェック
	if argument == '0':
		print '全タスクのリストを表示するよ'
		task_lists()
	elif argument == '1':
		print 'TODOを追加するよ！'
		task_add()
	elif argument == '2':
		print 'タスクを削除するよ'
		task_delete()
	elif argument == '3':
		print 'タスクを編集するよ'
		task_edit()
	elif argument == '4':
		print 'カラーについての早見表ヘルプ'
	elif argument == '5':
		print 'タスクの達成'
		achieveTask()
	elif argument == '6':
		print '終了します'
		sys.exit()
	elif argument == '7':
		print 'debug'
		debug_test()
	else:
		print '1〜5の数字の中から選択してね'

# タスクリスト表示
def task_lists():
	dbcur.execute("select * from todo where flag like '0'")
	results = dbcur.fetchall()
	# 二次元配列を展開する
	for result in results:
		# 要素を1つずつ表示する
		for col in result:
			print col,
		print ''
	print ''

# タスク追加
def task_add():
	print 'TODOを入力してね！'
	todo = inputCheck()
	created = today.strftime('%Y-%m-%d')
	day = '{:%d}'.format(today)
	month = '{:%m}'.format(today)
	year = '{:%Y}'.format(today)
	day = (int)(day)
	month = (int)(month)
	year = (int)(year)

	print '日付登録してね！(入力例:YYYY-MM-DD)\n今日　明日　明後日の単語でも登録出来るよ'
	# 日付登録時に明日や明後日、一週間後、一ヶ月後などの言葉でも登録できるようにする
	achieve_day = inputCheck()
	if achieve_day == 'today' or achieve_day == u'今日':
		achieve_day = created
	elif achieve_day == 'tomorrow' or achieve_day == u'明日':
		day += 1
		achieve_day = '{:%Y-%m}-{}'.format(today, day)
	elif achieve_day == u'明後日':
		day += 2
		achieve_day = '{:%Y-%m}-{}'.format(today, day)

	dbcur.execute("insert into todo (todo, flag, danger_flg, achieve_day, created) values (?, ?, ?, ?, ?)", (todo, False, False, achieve_day, created))
	dbcon.commit()
	print '登録完了したよ！'
	print ''

# タスク削除
def task_delete():
	while 1:
		print '削除するタスクIDを入力してね！\nもし、削除しない場合は、leaveって打ってね'
		idtodo_list()

		delete_num = inputCheck()
		if delete_num != 'leave':
			dbcur.execute("delete from todo where id = ?", (delete_num))
			dbcon.commit()
			print ''
		else:
			print ''
			break

# タスクの編集
def task_edit():
	print 'TODOを編集します。編集するTODOのIDをまず入力してね！'
	edit_db = "update todo set "

	idtodo_list()
	edit_num = inputCheck()
	
	if edit_num is not None:
		# データを取得する
		dbcur.execute("select id, todo, flag, achieve_day from todo where id = ?", (edit_num))
		result = dbcur.fetchone()
		if result is not None:
			for col in result:
				print col,
			print ''

			# 達成状況の変更
			print '達成状況を変更する？\n1:達成 0:未達成'
			edit_flg = inputCheck()
			if edit_flg is not None:
				while 1:
					if edit_flg == '1':
						print '「達成」にしました'
						break
					elif edit_flg == '0':
						print '「未達成」にしました'
						break
					else:
						print '1か0を入力してください'
						edit_flg = inputCheck()
				edit_db += "flag = ? "

			# 達成予定日の変更
			print '達成予定日を変更する？^\n記入フォーマット:YYYY-MM-DD'
			edit_achieveday = inputCheck()
			if edit_achieveday is not None:
				if edit_flg == '1' or edit_flg == '0':
					edit_db += ", "
				edit_db += "achieve_day = ? "

			# 変更を代入
			edit_db += "where id = ?"
			if edit_flg is None and edit_achieveday is None:
				print '特に変更はありません'
			elif edit_flg is not None:
				dbcur.execute(edit_db, (edit_flg, edit_num))
			elif edit_achieveday is not None:
				dbcur.execute(edit_db, (edit_achieveday, edit_num))
			else:
				dbcur.execute(edit_db, (edit_flg, edit_achieveday, edit_num))
			dbcon.commit()
			print ''
		else:
			print 'そのIDは見つかりませんでした'
	else:
		print ''

def achieveTask():
	print '達成したタスクのIDを入力してね'
	idtodo_list()
	achieve_task = inputCheck()
	if achieve_task is not None:
		dbcur.execute("update todo set flag = ? where id = ?", (True, achieve_task))
		dbcon.commit()

def debug_test():
	dbcur.execute("select * from todo")
	results = dbcur.fetchall()
	# 二次元配列を展開する
	for result in results:
		# 要素を1つずつ表示する
		for col in result:
			print col,
		print ''
	print ''

# IDとTODOを表示するやつ
def idtodo_list():
	dbcur.execute("select id, todo from todo")
	results = dbcur.fetchall()
	for result in results:
		for col in result:
			print col,
		print ''
	print ''

def color_help():
	print 'カラー早見表'

# 入力チェック
def inputCheck():
	inputKey = sys.stdin.readline().strip()
	# 入力があれば、デコードして返す。なければ、Falseで返す。
	if inputKey:
		return inputKey.decode('utf-8')
	else:
		return None

# Cで言うstatic void main(){}
if __name__ == '__main__':
	# 初期メニュー表
	while 1:
		print '半角数字でメニューを選択してください！(0〜5)'
		print '0: 全タスクリスト'
		print '1: TODO追加'
		print '2: タスクを削除する'
		print '3: タスク編集'
		print '4: カラー早見表'
		print '5: タスク達成チェック'
		print '6: 終了'
		print '7: debugテスト'
		menu_num = inputCheck()
		print ''
		menu(menu_num)
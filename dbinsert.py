# coding: utf-8
import sys, sqlite3, datetime

dbcon = sqlite3.connect('todo.db')
d = datetime.datetime.today()

todo = u'ダーツボードを買う'
flag = True
danger_flg = False
achieve_day = '2014-07-20'
created = d.strftime('%Y-%m-%d')

# id: 固有ID
# todo: すること
# flag: 達成済みか(trueになると達成)
# danger_flg: 未達成なもので期限が超えているか
# achieve_day: 達成させる予定日
# created: 作成日時

## テーブル作成
# dbcon.execute("create table todo (id integer primary key, todo text not null, flag bool not null, danger_flg bool not null, achieve_day varchar(50) not null, created varchar(50) not null)")

## テストTODO挿入
insert_column = 'insert into todo (todo, flag, danger_flg, achieve_day, created) values (?, ?, ?, ?, ?)'
dbcon.execute(insert_column, (todo, flag, danger_flg, achieve_day, created))
dbcon.commit()